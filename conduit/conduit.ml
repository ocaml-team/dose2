(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type 'channel conduit = {
  cd_out_channel : 'channel; 
  cd_print       : 'a . 'channel -> ('a, 'channel, unit) format -> 'a; 
  cd_flush       : 'channel -> unit; 
};;

let stdoutcd = {
  cd_out_channel = stdout;
  cd_print = Printf.fprintf;
  cd_flush = flush
};;

let stderrcd = {
  cd_out_channel = stderr;
  cd_print = Printf.fprintf;
  cd_flush = flush
};;

let conduit_of_channel oc = {
  cd_out_channel = oc;
  cd_print = Printf.fprintf;
  cd_flush = flush
};;

let conduit_of_buffer b =
  { cd_out_channel = b;
    cd_print = Printf.bprintf;
    cd_flush = ignore }
;;

let scribe_string cd oc u = cd.cd_print oc "%s" u;;
