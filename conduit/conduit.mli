(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** A conduit is a channel for using formatting functions on buffers or channels. *)
type 'a conduit = {
  cd_out_channel : 'a;                              (** Can be a buffer or a channel. *)
  cd_print : 'b. 'a -> ('b, 'a, unit) format -> 'b; (** The print function. *)
  cd_flush : 'a -> unit;                            (** The flush function. *)
}

val stdoutcd : out_channel conduit (** The conduit linked to standard output *)

val stderrcd : out_channel conduit (** The conduit linked to standard error *)

val conduit_of_channel : out_channel -> out_channel conduit (** Builds a conduit from an output channel. *)

val conduit_of_buffer : Buffer.t -> Buffer.t conduit (** Builds a conduit from a buffer. *)

val scribe_string : 'a conduit -> 'a -> string -> unit (** Writes a string into a conduit. *)
