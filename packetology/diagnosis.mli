(* Copyright 2005-2008 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Napkin
open Rapids

type ('package_id, 'versioned) reason =
| Not_available of 'package_id
| Requested of 'package_id
| Dependency of 'package_id * 'package_id list
| Empty_disjunction of 'package_id * 'versioned list
| Conflict of 'package_id * 'package_id

type ('not_installable_id, 'package_id, 'versioned) diagnosis = {
  dg_closure_size : int;
  dg_conflicts    : int;
  dg_disjunctions : int;
  dg_dependencies : int;
  dg_failures     : ('not_installable_id * ('package_id, 'versioned) reason list) array
}

val empty_diagnosis : ('a, 'b, 'c) diagnosis;;

val io_reason : 'a Io.literate -> 'b Io.literate -> ('a, 'b) reason Io.literate
val io_diagnosis : 'a Io.literate -> 'b Io.literate -> 'c Io.literate -> ('a, 'b, 'c) diagnosis Io.literate
