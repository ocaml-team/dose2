(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Napkin
open Rapids
open Io;;
module G = Generic_io;;

(*** key_of_archives *)
let key_of_archives archives =
  let al =
    List.fold_left
      begin fun key archive ->
        [archive.flavor; archive.architecture; archive.distribution; archive.component] :: key
      end
      []
      archives
  in
  let pl = Factorize.factorize al in
  Factorize.encode_product_list (fun b x -> Printf.bprintf b "%s" x) pl
;;
(* ***)
(*** store *)
class store db =
  let io_range =
    io_convert
      (fun (u_name, str_sel) ->
        let u_id = id_of_unit db u_name in
        (u_id,
         map_selector
           (fun v -> version_of_name db u_id v)
           str_sel))
      (fun (u_id,   num_sel) ->
        (name_of_unit db u_id,
         map_selector (name_of_version db u_id) num_sel))
    (io_pair
      io_string
      (io_selector io_string))
  in
  let io_diagnosis =
    Diagnosis.io_diagnosis
      (io_convert
        (fun (u,v) -> find_package_id db u v)
        (fun p_id -> unit_and_version_of_package db (get_package db p_id))
        (io_pair io_string io_string))
      io_range
  in
  object(self)
    method load fn = G.load_from_file io_diagnosis fn
    method save fn dg = G.save_to_file ~header:true ~fmt:G.Asc io_diagnosis dg fn
    (*** make_filename *)
    method make_filename path key date =
      Filename.concat
        path
        (Util.sanitize_filename
          ~is_safe:
             (function
               |'A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-'|'('|')'|'+'|','|'.' -> true
               | _ -> false)
          (sf "diagnosis.%s.%s" key (Util.string_of_iso_8601 date)))
    (* ***)
  end
(* ***)
