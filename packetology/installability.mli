(* Copyright 2005-2008 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Napkin
open Rapids
open Diagnosis

module N:
sig
	type t = (package_id, (unit_id, version_number * release_number, glob) versioned) reason
	type reason = t
end;;

(* This doesn't do anything, but is preserved for upwards compatibility with
 * unstable versions *)
type solver =
	SAT (* Jerome Vouillon's SAT-based solver *)
| Direct (* Direct solver *)

val set_solver: solver -> unit
val get_solver: unit -> solver

val set_verifier: solver option -> unit
val get_verifier: unit -> solver option

val prepare : 
	db ->
	?log:(string -> unit) ->
	indicator:Progress.indicator ->
	?targets:Package_set.t ->
	available:Package_set.t ->
	unit ->
	int * (Package_set.elt -> Solver.M(N).var) * (Solver.M(N).var -> Package_set.elt) * Package_set.t * Solver.M(N).state * 'a list ref * int ref * int ref * int ref

val check : 
    db ->
		?log:(string -> unit) ->
		?preparation:(int * (Package_set.elt -> Solver.M(N).var) * (Solver.M(N).var -> Package_set.elt) * Package_set.t * Solver.M(N).state * (Package_set.elt * N.reason list) list ref * int ref * int ref * int ref) ->
    indicator:Progress.indicator ->
    targets:Package_set.t ->
    available:Package_set.t ->
    unit ->
    (Package_set.elt, Package_set.elt, (Rapids.unit_id, (Rapids.version_number * Rapids.release_number), glob) Napkin.versioned) Diagnosis.diagnosis

val check_together : 
    db ->
		?log:(string -> unit) ->
		?preparation:int * (Package_set.elt -> Solver.M(N).var) * (Solver.M(N).var -> Package_set.elt) * Package_set.t * Solver.M(N).state * (Package_set.elt list * N.reason list) list ref * int ref * int ref * int ref ->
    indicator:Progress.indicator ->
    targets:Package_set.t ->
    available:Package_set.t ->
    unit ->
    (Package_set.elt list, Package_set.elt, (Rapids.unit_id, (Rapids.version_number * Rapids.release_number), glob) Napkin.versioned) Diagnosis.diagnosis

val install : 
    db ->
		?log:(string -> unit) ->
		?preparation:(int * (Package_set.elt -> Solver.M(N).var) *
		  (Solver.M(N).var -> Package_set.elt) * Package_set.t * Solver.M(N).state *
			'a list ref * int ref * int ref * int ref) ->
    indicator:Progress.indicator ->
    targets:Package_set.t ->
    available:Package_set.t ->
		?known:Package_set.t ->
    unit ->
		Package_set.t

val abundance : db -> Package_set.t -> package_id option

val strong_dep:
	db -> ?is:Package_set.t ->
	?preparation:int * (Package_set.elt -> Solver.M(N).var) * (Solver.M(N).var -> Package_set.elt) * Package_set.t * Solver.M(N).state * 'a list ref * int ref * int ref * int ref ->
	Package_set.t -> package_id -> package_id -> bool

val trim:
	db ->
	Package_set.t ->
	Package_set.t
