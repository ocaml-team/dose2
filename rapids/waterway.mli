(* Copyright 2005-2008 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** Glues RAPIDS and various parser modules to provide a convenient way of loading
    a RAPIDS database. *)

(** A waterway is a way to get the liquid we are interested in.  This can be in
    a DOSEbase(TM), a file (which will be a Debian APT list or an hdlist), or a
    structured collection of such files. *)
type waterway = File of string | Directory of string | Dose of string

(** It is possible to load multiple waterways at once, provided they have the same liquid.
    This is given as a list, whose components must all have the same [liquid].  Yes, this
    could have been factored but it is not very important and simpler that way. *)
type specification = (Rapids.liquid * waterway) list

(** The textual representation of a specifications is a comma-separated list as below:
  
    (deb|dose|rpm|doserpm):filename(,(deb|dose|rpm|doserpm):filename)*

    For example:

    dose:/tmp/dose,deb:/var/lib/apt/lists/ftp.fr.debian.org_debian_dists_testing_main_binary-i386_Packages

    is a valid specification. *)

val specification_of_string : string -> specification

(** Merge a waterway into the given database. *)
val merge : Rapids.db -> ?progress:Progress.indicator ->
	?add_file_conflicts:bool -> ?overlay:bool -> specification -> unit

(** Call [with_database sl handler] where [sl]
    is a waterstream specification and [handler] is your function. *)
(* val with_database : specification -> (Rapids.db -> unit) -> unit *)
