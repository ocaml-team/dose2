(* Copyright 2006 Berke DURAK and INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** Accessing memory-mapped files as strings.

    This module allows one to map files into memory and access
    them byte by byte, or by extracing or blitting substrings.

*)

type t (** A type representing a memory-mapped file *)
val map : ?write:bool -> ?copy:bool -> ?offset:int64 -> ?length:int64 -> Unix.file_descr -> t
  (** [map fd copy offset length] maps the bytes [offset .. offset + length - 1] of file [fd];
      if [copy] is true, the file is mapped in copy-on-write mode. *)
val unmap : t -> unit
  (** [unmap mp] removes the mapping [mp] ; all subsequent calls with [mp] will raise an [Invalid_argument]
      exception. *)
val sub : t -> int64 -> int -> string
  (** [sub mp o n] copies the bytes from [offset + o] to [offset + o + n]
      into a new string *)
val length : t -> int64
  (** Returns the length of the mapped region *)
val offset : t -> int64
  (** Returns the offset of the mapped region *)
val blit : t -> int64 -> string -> int -> int -> unit
  (** [blit mp i u j n] copies the bytes
      [j] to [j + n - 1] of the string [u]
      into the bytes [offset + i] to [offset + i + n - 1] of the map. *)
val paste : t -> int64 -> string -> unit
  (** [paste mp i u] copies the string [u] into the bytes [offset + i] to [offset + i + n - 1]
      of the map, where [n] is the length of the string [u]. *)
  (*
val get : t -> int64 -> char
  (** [get mp i] returns the byte at [offset + i] of the mapped file t as a character *)
val get_byte : t -> int64 -> int
  (** [get_byte mp i] returns the byte at [offset + i] of the mapped file t as an integer *)
val blit_from_string : t -> int64 -> int -> string -> int -> unit
  (** [blit_from_string mp i n u j] copies the bytes
      [j] to [j + n - 1] of the string [u]
      into the bytes [offset + i] to [offset + i + n - 1] of the map. *)
  *)
