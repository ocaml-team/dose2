(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type t

external caml_map : bool -> bool -> int64 -> int64 -> Unix.file_descr -> t = "caml_mmap_map" "caml_mmap_map"

module UL = Unix.LargeFile;;

let map ?(write=false) ?(copy=false) ?(offset=0L) ?length fd =
  let st = UL.fstat fd in
  let length = Int64.sub st.UL.st_size offset in
  caml_map write copy offset length fd
;;

external unmap : t -> unit = "caml_mmap_unmap" "caml_mmap_unmap"
external length : t -> int64 = "caml_mmap_length" "caml_mmap_length"
external offset : t -> int64 = "caml_mmap_offset" "caml_mmap_offset"
external sub : t -> int64 -> int -> string = "caml_mmap_sub" "caml_mmap_sub"
external blit : t -> int64 -> string -> int -> int -> unit = "caml_mmap_blit" "caml_mmap_blit"

let paste mp offset u =
  let n = String.length u in
  blit mp offset u 0 n
;;
