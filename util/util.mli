(* Copyright 2004-2007 Berke DURAK 

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

val sf : ('a, unit, string) format -> 'a
val first_line : string -> string
val limit : int -> string -> string
val limit_left : int -> string -> string
val for_all_chars : (char -> bool) -> string -> bool
val split_once_at : (char -> bool) -> string -> string * string
val is_alpha : char -> bool
val is_digit : char -> bool
val is_space : char -> bool
val parse_strings : string -> string list
val split_at : char -> string -> string list
val list_intersect : 'a list -> 'a list -> 'a list
val once : (unit -> unit) -> unit -> unit
val list_has_more_than_one_element : 'a list -> bool
val count_lines : string -> int
val first_matching_char_from : int -> (char -> bool) -> string -> int
val first_matching_char : (char -> bool) -> string -> int
val first_matching_char_rev_from : int -> (char -> bool) -> string -> int
val first_matching_char_rev : (char -> bool) -> string -> int
val remove_leading_spaces : string -> string
val remove_spaces : string -> string
val delete_first_chars : int -> string -> string
val longest_matching_prefix : (char -> bool) -> string -> string * string
val hierarchical : string -> string -> int
val wind : ('a -> 'b) -> 'a -> ('c -> 'd) -> 'c -> 'b
val list_change_nth : 'a list -> int -> 'a -> 'a list
val list_remove_nth : 'a list -> int -> 'a list
val word_wrap : out_channel -> ?columns:int -> string -> unit
val reg_of_string : string -> string
val flip_array : 'a array -> unit
val proc_get_rsz_vsz : unit -> int * int
val proc_get_free_mem : unit -> int
val substitute_variables : (string * string) list -> string -> string
val string_of_process_status : string -> Unix.process_status -> string option
val list_sub_rev : 'a list -> int -> int -> 'a list
val unix_really_read : Unix.file_descr -> string -> int -> int -> unit
val is_prefix : string -> string -> bool
val string_of_sockaddr : Unix.sockaddr -> string
val sockaddr_of_string : string -> Unix.sockaddr
val int64_of_inet_addr : Unix.inet_addr -> int64
val lowercase_compare : string -> string -> bool
val call_if : ('a -> unit) option -> 'a -> unit
val wrap : 'a -> ('a -> 'b) -> ('a -> 'c) -> 'c
val string_of_iso_8601 : (int * int * int) -> string
val seconds_of_iso_8601 : (int * int * int) -> float
val iso_8601_of_string : string -> (int * int * int)
val binary_search : ('a -> 'a -> int) -> 'a array -> 'a -> int
val randomize : 'a array -> unit
val array_mem_assoc : 'a -> ('a * 'b) array -> bool
val array_assoc : 'a -> ('a * 'b) array -> 'b
val today : unit -> (int * int * int)
val mkdirhier : string -> unit
val inside : string -> ('a -> 'b) -> 'a -> 'b
val catch : (unit -> 'a) -> 'a
val sanitize_filename : ?is_safe:(char -> bool) -> ?buffer:Buffer.t -> ?prefix:string -> string -> string
val unsanitize_filename : ?buffer:Buffer.t -> ?prefix:string -> string -> string
(*val under_lock : Mutex.t -> ('a -> 'b) -> 'a -> 'b*)
val with_output_to_file : string -> (out_channel -> 'a) -> 'a
val uniq: 'a list -> 'a list
val string_contains: string -> string -> bool
