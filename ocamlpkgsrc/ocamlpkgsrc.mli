(* Copyright 2005-2008 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER and the
EDOS Project.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

val compare_versions: string -> string -> int

val read_summary_file: string -> Napkin.default_package list

type fnmatch_options =
  FNM_NOESCAPE 
| FNM_PATHNAME
| FNM_PERIOD

val fnmatch: string -> string -> fnmatch_options list -> bool
