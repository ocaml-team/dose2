(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt, Jaap BOENDER.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

val decompose_key : string -> string * string list
module Out :
  sig
    type t
    val open_out : ?set_type:string -> string -> t
    val close_out : t -> unit
    val add_package :
      t ->
      archive:string ->
      day:Lifetime.day ->
      (string * string * string list) list ->
      unit
    val rebuild_index : string -> unit
  end
module In :
  sig
    type t
    type package
    val open_in : string -> t
    val close_in : t -> unit
		val count_population: t -> int 
		val get_type: t -> string
    val iter :
      ?package:(unit_name:string -> version:string -> architecture:string -> package -> unit) ->
      ?lifetime:(archive:string -> unit_name:string -> version:string ->
			           architecture:string -> Lifetime.lifetime -> unit) ->
      t ->
      unit
    val iter_over_fields : (string * string * string list -> unit) -> t -> package -> unit
    val find_package : t -> unit_name:string -> version:string -> architecture:string -> package
    val find_lifetime : t -> architecture:string -> archive:string -> unit_name:string -> version:string -> Lifetime.lifetime
    val get_package_id : t -> package -> int
    val get_field : t -> package -> string -> string * string list
    val get_field_first : t -> package -> string -> string
    val known_fields : t -> string list
  end

(** [napkin_of_fields assoc] parses a Dosebase package description given by a
function [assoc] that takes a field name and returns the first line of the
contents of the field. *)
val napkin_of_fields: (string -> string) -> Napkin.default_package
