(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** I/O functions for binary data. *)

val out_of_channel : out_channel:out_channel -> ?compress:bool -> unit -> Fragments.io_out
  (** Create a reader from an Ocaml input channel, which must be open in binary mode. *)
val in_of_channel : in_channel:in_channel -> ?compress:bool -> unit -> Fragments.io_in
  (** Create a writer from an Ocaml output channel, which must be open in binary mode. *)
val in_of_string : str:string -> ?offset:int -> ?length:int -> ?compress:bool -> unit -> Fragments.io_in
  (** Create a reader from an Ocaml string. *)
val out_to_string : ?size:int -> ?compress:bool -> unit -> Fragments.io_out * (unit -> string)
  (** Create a writer fo a string.  Returns the writer and a function that will return the generated string. *)
