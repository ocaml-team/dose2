(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

type file_format =
| Bin
| Cbn
| Txt
| Asc

exception Unknown_format

val format_list : (file_format * string) list

val format_to_id : file_format -> string
val id_to_format : string -> file_format

val in_from_channel : ?header:bool -> ?fmt:file_format -> in_channel -> Fragments.io_in
val out_from_channel : ?header:bool -> ?fmt:file_format -> out_channel -> Fragments.io_out

val save_to_file : ?header:bool -> ?fmt:file_format -> 'a Io.literate -> 'a -> string -> unit
val save_to_channel : ?header:bool -> ?fmt:file_format -> 'a Io.literate -> 'a -> out_channel -> unit
val load_from_file : 'a Io.literate -> string -> 'a
val load_from_channel : ?header:bool -> ?fmt:file_format -> 'a Io.literate -> in_channel -> 'a

val to_string : ?header:bool -> ?fmt:file_format -> 'a Io.literate -> 'a -> string
val from_string : ?header:bool -> ?fmt:file_format -> ?offset:int -> 'a Io.literate -> string -> 'a
val in_from_string : ?header:bool -> ?fmt:file_format -> ?offset:int -> string -> Fragments.io_in
