(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** I/O functions for human-readable, pretty-printed data. *)

val in_of_stream : stream:char Stream.t -> unit -> Fragments.io_in
  (** Create a reader from a character stream. *)
val in_of_channel : in_channel:in_channel -> unit -> Fragments.io_in
  (** Create a reader from an Ocaml input channel. *)
val out_of_formatter : formatter:Format.formatter -> ?indent:bool -> unit -> Fragments.io_out
  (** Create a writer from a formatter. *)
val out_of_channel : ?condensed:bool -> out_channel:out_channel -> ?indent:bool -> unit -> Fragments.io_out
  (** Create a writer from an Ocaml output channel. *)
