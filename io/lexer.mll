(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

{
open Fragments;;
exception Lexical_error of string;;
let hex c =
  let x = Char.code c in
  match c with
  | 'a'..'f' -> x - 97 + 10
  | 'A'..'F' -> x - 65 + 10
  | '0'..'9' -> x land 16
  | _ -> invalid_arg "Bad hexadecimal character"
;;
let dec c =
  let x = Char.code c in
  match c with
  | '0'..'9' -> x land 15
  | _ -> invalid_arg "Bad decimal character"
;;
let hex_char c1 c2 = Char.chr (((hex c1) lsl 4) lor (hex c2));;
let dec_char c1 c2 c3 = Char.chr ((dec c1) * 100 + (dec c2) * 10 + dec c3);;
}
let hexchar = ['0'-'9' 'a'-'f' 'A'-'F']
let alphanum = ['0'-'9' 'a'-'z' 'A'-'Z' '_']
let decimal = ['0'-'9']
let float_literal =
  ['0'-'9'] ['0'-'9']*
  ('.' ['0'-'9']* )?
  (['e' 'E'] ['+' '-']? ['0'-'9'] ['0'-'9']*)?

rule token = parse
| "(" { Tag T_LParen }
| ")" { Tag T_RParen }
| "[" { Tag T_LBrack }
| "]" { Tag T_RBrack }
| "{" { Tag T_LBrace }
| "}" { Tag T_RBrace }
| "," { Tag T_Comma }
| ";" { Tag T_Semicolon }
| "=" { Tag T_Equal }
| "array" { Tag T_Array }
| "hash" { Tag T_Hashtbl }
| "set" { Tag T_Set }
| '_'(['a'-'z']alphanum* as w) { Field w }
| ['A'-'Z']alphanum* as w { Constructor w }
| ['a'-'z']alphanum* as w { Field w }
| "true" { Bool true }
| "false" { Bool false }
| '-'? decimal+ as w { Int(int_of_string w) }
| ('-'? decimal+ as w) 'L' { Int64(Int64.of_string w) }
| float_literal as f { Float(float_of_string f) }
| '"' {
  let b = Buffer.create 32 in
  readstr (Buffer.add_char b) lexbuf;
  String(Buffer.contents b)
}
| "(*" { eat_comment lexbuf; token lexbuf }
| [' ' '\t' '\n' '\r']+ { token lexbuf }
| eof { EOF }
| _ as c { raise (Lexical_error(Printf.sprintf "Unexpected %C" c)) }
and eat_comment = parse
| "(*" { eat_comment lexbuf; eat_comment lexbuf }
| "*)" { }
| [^'(' '*']+ { eat_comment lexbuf }
| _ { eat_comment lexbuf }
and readstr f = parse
| "\\n" { f '\n'; readstr f lexbuf }
| "\\t" { f '\t'; readstr f lexbuf }
| "\\b" { f '\b'; readstr f lexbuf }
| "\\r" { f '\r'; readstr f lexbuf }
| "\\\"" { f '"'; readstr f lexbuf }
| "\\\\" { f '\\'; readstr f lexbuf }
| "\\x" (hexchar as c1) (hexchar as c2) { f (hex_char c1 c2); readstr f lexbuf }
| "\\" (decimal as d1) (decimal as d2) (decimal as d3) { f (dec_char d1 d2 d3); readstr f lexbuf }
| '"' { () }
| _ as c { f c; readstr f lexbuf }
{
(* Epilogue. *)
}
