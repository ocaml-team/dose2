(* Copyright 2005-2007 Berke DURAK, INRIA Rocquencourt.

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** A progress indicator module *)

class type progress_indicator =
  object
    method set_label : string -> unit
    method set_decimation : int -> unit
    method display : ?force:bool -> int * int -> unit
    method start : unit
    method finish : unit
    method warn : string -> unit
  end
;;

class indicator : decimation:int -> label:string -> channel:out_channel -> unit -> progress_indicator
class formatter_indicator : decimation:int -> label:string -> fmt:Format.formatter -> unit -> progress_indicator
val dummy : progress_indicator
