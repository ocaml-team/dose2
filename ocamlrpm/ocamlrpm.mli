(* Copyright 2005-2008 Jaap Boender.

This file is part of Dose2.

Dose2 is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Dose2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Dose2; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

open Napkin

val compare_versions: string -> string -> int

val add_file_provides: Progress.indicator -> package_with_files list -> package_with_files list
val add_file_conflicts: Progress.indicator -> package_with_files list -> package_with_files list

val read_package: string -> string -> package_with_files
val read_hdlist: string -> string -> package_with_files list
val read_synthesis_hdlist: string -> package_with_files list
