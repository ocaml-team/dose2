(* test for reading an writing debian package files *)

open Napkin;;
open Ocamldeb;;
open Arg;;

let inputfile = ref "";;

let options = 
  [ ( "-i",
      Set_string(inputfile),
      "path of the input Packages file"
    )
  ]
in
  parse
      options
	(function _ -> raise (Bad "Argument not allowed"))
	"uplose -i <inputfile>";;

if (!inputfile = "") then
  raise (Bad "Missing input file");;

let
    packages = read_pool_file !inputfile Progress.dummy
in
  Ocamldebwriter.output_package_list stdout (List.rev packages);;
