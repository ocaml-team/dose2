(****************************************************************************

Copyright 2009 Ralf Treinen <treinen@pps.jussieu.fr>

This file is part of Dose2.

Dose2 is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Dose2 is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see <http://www.gnu.org/licenses/>.

***************************************************************************)

(* Writing out in debian/control format napkin package descriptions *)
(* from debian data. Conforms to debian policy version 3.8.0.1      *)
(* See below for an exact listing of printed field.                 *)

(* write one package *)
val output_package: out_channel -> Napkin.default_package -> unit

(* write a list of packages, each of them terminated by \nl *)
val output_package_list: out_channel -> Napkin.default_package list -> unit 

(* The following fields are written: *)
(* - Package                         *)
(* - Version                         *)
(* - Architecture                    *)
(* - Size                            *)
(* - Installed_Size                  *)
(* - Essential   [only if true]      *) 
(* - Source                          *)
(* - Provides    [only if nonempty]  *)
(* - Conflicts   [only if nonempty]  *)
(* - Breaks      [only if nonempty]  *)
(* - Replaces    [only if nonempty]  *)
(* - Depends     [only if nonempty]  *)
(* - Pre-Depends [only if nonempty]  *)
(* - Suggests    [only if nonempty]  *)
(* - Recommends  [only if nonempty]  *)
(* - Enhances    [only if nonempty]  *)

